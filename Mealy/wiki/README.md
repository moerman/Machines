Machines from the automata wiki
===============================

These are Mealy models for the machines listed on the automata learning wiki.
Of course these machines are not Mealy machines, but register machines to some
extend. To cope with that we used the following commands to restrict the values
and flatten to Mealy machines. (Only the result is stored in this repo.)

	sut_uppaal2bcg model.uppaal.xml model.bcg 0 2
	bcg_io model.bcg model.aut
	sut_ioaut2mealydot model.aut model.dot

where 0 and 2 is the range of the data parameters (I believe). Note that 0
might have a special meaning.

For now I've converted Boundary retransmission protocol, Session initiation
protocol and the Biometric passport.

[Automata wiki](http://www.mbsd.cs.ru.nl/automata/)

