ESM controller (and hypotheses)
===============================

The mealy machine here is based on the RRRT-model as described in the SMVJ15
paper. It is flattened to be a Mealy machine. The sub directories list different
runs of the learning algorithm and contain all the hypotheses. This gives a nice
sequence of machines, growing in size.

- run1 was performed by Wouter, the last few hypothesis were constructed with
  white-box testing.
- run2 (incomplete) and run3 are performed by Joshua.
